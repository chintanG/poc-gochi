package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"maas-policies/models"
	"maas-policies/services"
	"net/http"
)

/*
	descritptopns:
		Create policy handler provides handler to create policy
	returns:
		request handelr
*/
func CreatePolicy() http.HandlerFunc {
	fmt.Println("Creating policy")
	log.Println("Creating policy")
	return func(w http.ResponseWriter, r *http.Request) {

		request := &models.Policy{}
		json.NewDecoder(r.Body).Decode(&request)

		// feed.Add(newsfeed.Item{
		// 	Title: request["title"],
		// 	Post:  request["post"],
		// })
		w.Write([]byte("Good job!"))
		policy := services.GeneratePolicy(request)
		json.NewEncoder(w).Encode(policy)
	}

}
