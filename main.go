package main

import (
	"fmt"
	"maas-policies/handler"
	"net/http"

	"github.com/go-chi/chi"
)

func registerRoutes(router *chi.Mux) {
	router.Route("/policies", func(r chi.Router) {
		r.Post("/", handler.CreatePolicy())
	})
}

func main() {
	port := ":8089"
	router := chi.NewRouter()
	registerRoutes(router)
	fmt.Println("addr :={}", port)
	http.ListenAndServe(port, router)
}
