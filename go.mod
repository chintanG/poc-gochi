module maas-policies

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/render v1.0.1 // indirect
	go.mongodb.org/mongo-driver v1.4.2
)
