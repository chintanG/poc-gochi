package models

import (
	"net/http"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Getter interface {
	// GetAll() PolicyList
	Get() Policy
}

type Policy struct {
	ID          primitive.ObjectID `json:"id" bson:"_id"`
	name        string             `json:"name" bson:"Name,omitempty"`
	description string             `json:"description" bson:"Description,omitempty"`
	createdAt   string             `json:"created_at" bson:"created_at,omitempty"`
}

// func (i *Policy) Bind(r *http.Request) error {
// 	if i.ID == "" {
// 		return fmt.Errorf("name is a required field")
// 	}
// 	return nil
// }

type PolicyList struct {
	Policy []Policy `json:"policies"`
}

func (*PolicyList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
func (*Policy) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
