package dao

import (
	"context"
	"log"
	"maas-policies/models"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const dbname string = "jrplanner"
const connectionstring string = ""

var dbConnector = configMongoDb()

func configMongoDb() *mongo.Database {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*120)
	defer cancel()

	clientOptions := options.Client().ApplyURI(connectionstring).SetDirect(true)

	c, err := mongo.NewClient(clientOptions)
	err = c.Connect(ctx)
	if err != nil {
		log.Fatalf("unable to initialize connection %v", err)
	}

	err = c.Ping(ctx, nil)
	if err != nil {
		log.Fatalf("unable to connect %v", err)
	}
	dbConnector := c.Database(dbname)
	return dbConnector
}

type Error struct {
	message string
}

type ResponseOrError struct {
	data *mongo.InsertOneResult
	err  *Error
}

func InsertOne(policy *models.Policy) *ResponseOrError {
	log.Println("dao.InsertOne")
	collection := dbConnector.Collection("Region23")
	policy.ID = primitive.NewObjectID()
	result, err := collection.InsertOne(context.TODO(), policy)
	var response *ResponseOrError = nil
	if err != nil {
		log.Printf("Could not Insert Record: %v", err)
		message := "Object Insertion failed"
		errorObj := &Error{message: message}
		response.err = errorObj
	} else {
		response.data = result
	}
	return response
}
